/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on September 26, 2013, 4:22 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    int year;
    
    cout << "Enter a year.\n";
    cin >> year;
            
    
    if ((year % 400 == 0)  || (year % 4 == 0) && (year % 100 != 0))
    {
        cout << "This is a leap year";
    }
    else 
    {
        cout << "Not a leap year."; 
    }
        
    
    

    return 0;
}

